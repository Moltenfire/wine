from sklearn.datasets import load_iris
import pandas as pd
from neural_network_helpers import indices_to_one_hot

def get_iris_df():
    return pd.read_csv("iris.csv")

    # iris=load_iris()
    # df = pd.DataFrame.from_records(iris['data'], columns=iris['feature_names'])
    # df['species'] = iris['target_names'][iris['target']]
    # return df

def get_irsi_ml():
    iris=load_iris()

    xs = iris['data'][:100, :]
    ys = iris['target'][:100]

    # print(xs.shape)
    # print(ys.shape)

    return xs, ys
