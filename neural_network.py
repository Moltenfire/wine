import numpy as np
from itertools import tee 

def get_random(size, min_val=0, max_val=1):
  return np.random.random_sample(size) * (max_val - min_val) + min_val

def sigmoid(x):
  return 1.0 / (1.0 + np.exp(-x))

def sigmoid_prime(x):
    return sigmoid(x) * (1 - sigmoid(x))

def pairwise(iterable):
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)

class NeuralNetwork():

    def __init__(self, sizes, seed=0):
        np.random.seed(seed)
        self.sizes = sizes
        self.num_layers = len(sizes)
        self.weights = []
        self.biases = []
        self.epoch = 0
        for s1, s2 in pairwise(self.sizes):
            w = get_random((s2, s1), -1, 1)
            b = get_random(s2, -1, 1)
            self.weights.append(w)
            self.biases.append(b)

    def forward(self, a):
        for weights, biases in zip(self.weights, self.biases):
            a = sigmoid(np.dot(weights, a) + biases)
        return a

    def train(self, xs, ys):
        self.epoch += 1
        learning_rate = 0.1
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]

        for x, y in zip(xs, ys):
            nabla_b_temp, nabla_w_temp = self.backwards(x, y)
            for layer in range(self.num_layers - 1):
                nabla_b[layer] += nabla_b_temp[layer]
                nabla_w[layer] += nabla_w_temp[layer]

        for layer in range(self.num_layers - 1):
            nabla_b[layer] /= len(xs)
            nabla_w[layer] /= len(xs)
            nabla_b[layer] *= learning_rate
            nabla_w[layer] *= learning_rate

        for layer in range(self.num_layers - 1):
            self.weights[layer] += nabla_w[layer]
            self.biases[layer] += nabla_b[layer]

    def backwards(self, x, y):
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        # forward pass
        ax = [x]
        zs = []
        for weights, biases in zip(self.weights, self.biases):
            z = np.dot(weights, ax[-1]) 
            z += biases
            a = sigmoid(z)
            zs.append(z)
            ax.append(a)

        # last layer
        errors = y - ax[-1]
        deltas = errors * sigmoid_prime(zs[-1])
        nabla_b[-1] = deltas
        nabla_w[-1] = np.dot(np.array([deltas]).T, np.array([ax[-2]]))

        # layers 2 to n-1
        for layer in range(2, self.num_layers):
            prev_deltas = deltas

            sp = np.array([sigmoid_prime(zs[-layer])]).T
            weights = self.weights[-layer+1]
            d = np.dot(weights.transpose(), np.array([prev_deltas]).transpose())
            deltas = d * sp

            nabla_b[-layer] = deltas.T[0]

            w = nabla_w[-layer]
            a = np.array([ax[-layer-1]])
            res = np.dot(deltas, a)

            nabla_w[-layer] = res

        return nabla_b, nabla_w
