from irsi import get_irsi_ml
from neural_network import NeuralNetwork
from neural_network_helpers import accuracy, plot, indices_to_one_hot
import numpy as np

xs, ys = get_irsi_ml()

training_xs = xs
training_ys = indices_to_one_hot(ys, 2)
test_xs = xs
test_ys = ys

layers = (4, 16, 2)

for i in range(100):
    nn = NeuralNetwork(layers, seed=i)
    a, r = accuracy(nn, test_xs, test_ys)
    print(i, a)
results = []

# a, r = accuracy(nn, test_xs, test_ys)
# print(a)
# results.append(r)

# for i in range(15):
#     for _ in range(1):
#         nn.train(training_xs, training_ys)

#     a, r = accuracy(nn, test_xs, test_ys)
#     print(a)
#     results.append(r)

# plot(test_xs, results)