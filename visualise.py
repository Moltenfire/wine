import seaborn as sns
import matplotlib.pyplot as plt
from irsi import get_iris_df
sns.set_style("ticks")

df = get_iris_df()

g = sns.pairplot(df, hue='species')
g = g.map_lower(sns.kdeplot)

plt.show()
