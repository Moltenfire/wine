import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from neural_network.nn_mat import Network as NeuralNetwork
from neural_network.neural_network_helpers import mse
from tqdm import tqdm
sns.set_style("darkgrid")

df_red = pd.read_csv("winequality-red.csv", sep=';')
df_white = pd.read_csv("winequality-white.csv", sep=';')

df_red['type'] = 1
df_white['type'] = 0

df_wines = df_red.append(df_white, ignore_index=True)


# Specify the data 
x = df_wines.iloc[:,0:11]

# Specify the target labels and flatten the array 
y = np.ravel(df_wines.type)

# Split the data up in train and test sets
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.33, random_state=0)

scaler = StandardScaler().fit(x_train)

x_train = scaler.transform(x_train)
x_test = scaler.transform(x_test)


epochs = 500
layers = (11, 12, 8, 1)


errors = []
nn = NeuralNetwork(layers)
error = mse(nn, x_test, y_test)
errors.append(error)

for _ in tqdm(range(epochs)):
    nn.train(x_train, y_train)
    error = mse(nn, x_test, y_test)
    errors.append(error)

plt.plot(errors)

plt.show()
